import 'package:flutter/material.dart';
import 'timer.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
        home: new Scaffold(
            appBar: AppBar(
              title: Text("Timer App"),
            ),
            body: Center(
                child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                Row(mainAxisAlignment: MainAxisAlignment.center, children: [
                  MyHomePage(0xfff44336),
                  MyHomePage(0xFFFFEB3B),
                ]),
                Row(mainAxisAlignment: MainAxisAlignment.center, children: [
                  MyHomePage(0xff8BC34A),
                  MyHomePage(0xff3F51B5),
                ]),
              ],
            ))));
  }
}
