import 'dart:async';
import 'package:flutter/cupertino.dart';
import 'package:intl/intl.dart';
import 'package:flutter/material.dart';
import 'package:flutter_datetime_picker/flutter_datetime_picker.dart';
import 'package:audioplayers/audio_cache.dart';

class MyHomePage extends StatefulWidget {
  var _color;

  MyHomePage(this._color, {Key key}) : super(key: key);

  @override
  _MyHomePageState createState() => _MyHomePageState(_color);
}

class _MyHomePageState extends State<MyHomePage> {
  String _startTime = '';

  int _counter = 10;
  var _timer = '00:00:00';

  var color;

  Timer _downTimer;

  _MyHomePageState(this.color);

  Future<void> _setTime() async {
    DatePicker.showTimePicker(context,
        theme: DatePickerTheme(), showTitleActions: true, onConfirm: (time) {
      var now = new DateTime.now(), diff = time.difference(now);

      _timer = diff.toString().split('.')[0];

      _startTime = new DateFormat.Hms().format(now).toString();

      if (_downTimer != null) {
        _downTimer.cancel();
      }

      _startTimer();

      setState(() {});
    }, currentTime: DateTime.now(), locale: LocaleType.en);
    setState(() {});
  }

  void _startTimer() {
    var dateInFormatText = _timer.split(':');
    _counter = int.parse(dateInFormatText[0]) * 3600 +
        int.parse(dateInFormatText[1]) * 60 +
        int.parse(dateInFormatText[2]);

    _downTimer = Timer.periodic(Duration(seconds: 1), (timer) {
      setState(() {
        if (_counter > 0) {
          if (_counter == 600) {
            _playAlarm();
          }

          _counter--;
          _DownTick();
        } else {
          _playAlarm();
          _downTimer.cancel();
        }
      });
    });
  }

  void _playAlarm() {
    AudioCache player = new AudioCache();
    const alarmAudioPath = "apex.mp3";
    player.play(alarmAudioPath);
  }

  void _DownTick() {
    print(_timer);
    String timer = _timer;
    var dateInFormatText = timer.split(":");

    DateTime dateResult = new DateTime.utc(
        1,
        1,
        0,
        int.parse(dateInFormatText[0]),
        int.parse(dateInFormatText[1]),
        int.parse(dateInFormatText[2]));

    _timer = new DateFormat.Hms()
        .format(dateResult.add(Duration(seconds: -1)))
        .toString();
  }

  @override
  Widget build(BuildContext context) {
    return new Container(
        margin: EdgeInsets.only(top: 30, left: 10),
        width: 165.0,
        height: 165.0,
        decoration: new BoxDecoration(
          borderRadius: new BorderRadius.circular(100.0),
          border: new Border.all(
            width: 17.0,
            color: Color(color),
          ),
        ),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            (_counter > 0)
                ? Text("")
                : Text(
                    "DONE!",
                    style: TextStyle(
                      color: Colors.green,
                      fontWeight: FontWeight.bold,
                      fontSize: 24,
                    ),
                  ),
            Text(
              '$_startTime',
              style: TextStyle(
                fontWeight: FontWeight.bold,
                fontSize: 22,
              ),
            ),
            Text(
              '$_timer',
              style: TextStyle(
                fontWeight: FontWeight.bold,
                fontSize: 22,
              ),
            ),
            FlatButton(
                onPressed: _setTime,
                child: Text(
                  'Set time',
                  style: TextStyle(color: Colors.blue),
                )),
          ],
        ));
  }
}
