import 'package:flutter/material.dart';
import 'package:timer_flutter/model/timer_model.dart';
import 'package:timer_flutter/service/timer_service.dart';
import 'package:states_rebuilder/states_rebuilder.dart';

class TimerCard extends StatelessWidget {
  Color _color;
  String _colorName;

  TimerCard(this._color, this._colorName);

  String format(Duration duration) {
    return duration.toString().split('.').first.padLeft(8, "0");
  }

  Future<Null> _selectTime(BuildContext context, model) async {
    final TimeOfDay picked = await showTimePicker(
      context: context,
      initialTime: TimeOfDay.now(),
    );
    if (picked != null) {
      TimeOfDay nowTime = TimeOfDay.now();
      model.setState((TimerModel s) {
        s.startTime = Duration(
          hours: nowTime.hour,
          minutes: nowTime.minute,
        );
        s.finishTime = Duration(
          hours: picked.hour,
          minutes: picked.minute,
        );

        s.cancelAllTimers();
        s.updateCurrentTime();
        s.startTimer();
      });
    }
  }

  @override
  Widget build(BuildContext context) {
    final model = RM.get<TimerModel>(name: _colorName);
    model.undoStackLength = 2;

    return Padding(
      padding: const EdgeInsets.only(top: 10, left: 5, right: 5),
      child: Container(
        decoration: new BoxDecoration(
          boxShadow: [
            BoxShadow(
              color: Colors.grey.withOpacity(.5),
              blurRadius: 20.0,
              spreadRadius: 0.0,
              offset: Offset(
                5.0,
                15.0,
              ),
            )
          ],
        ),
        height: 100,
        child: Card(
            shadowColor: Colors.black,
            shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.circular(10.0),
            ),
            child: ListTile(
              // 1й вариант использования rebuilder'а
              title: StateBuilder(
                  observe: () => model,
                  builder: (context, rm) {
                    return Text(format(rm.state.startTime));
                  }),
              // 2й вариант использования rebuilder'а
              // title: model.rebuilder(
              //   () => Text('${model.state.startTime}'),
              // ),
              subtitle: StateBuilder(
                observe: () => model,
                builder: (context, rm) {
                  // ???
                  return rm.whenConnectionState(
                    onIdle: () => Text(format(Duration()).toString()),
                    onWaiting: () => Text('waiting...'),
                    onError: (e) {
                      return rm.undoState().state.currentTime;
                    },
                    onData: (d) => Text(d.currentTime.toString()),
                  );
                },
              ),
              trailing: SizedBox(
                height: 50,
                width: 50,
                child: StateBuilder(
                    observe: () => model,
                    builder: (context, rm) {
                      return CircularProgressIndicator(
                        strokeWidth: 5,
                        value: rm.state.circularValue,
                        backgroundColor: Colors.grey.shade300,
                        valueColor: AlwaysStoppedAnimation<Color>(_color),
                      );
                    }),
              ),
              onTap: () => {
                _selectTime(context, model),
              },
            )),
      ),
    );
  }
}
