import 'dart:async';
import 'package:timer_flutter/model/timer_model.dart';
import 'package:states_rebuilder/states_rebuilder.dart';

extension TimerService on TimerModel {
  Duration _getDuration() {
    return this.finishTime - this.startTime + Duration(seconds: 1);
  }

  void startTimer() {
    this.circularValue = 1.0;
    createTimer();
    createPeriodicTimer();
  }

  void createTimer() async {
    Duration duration = _getDuration();
    if (duration.isNegative) {
      throw new Exception('Negative duration for timer');
    }
    Timer timer = Timer(duration, handleTimeout);
    this.timer = timer;
  }

  void start(Timer timer) {
    timer.tick;
  }

  void handleTimeout() {
    final model = RM.get<TimerModel>(name: this.color);
    cancelTimer(this.periodicTimer);
    model.setState((s) => {s.circularValue = 0.0});
    print('Alarm ${this.color}');
  }

  void createPeriodicTimer() async {
    if (this.timer != null && this.timer.isActive == true) {
      final model = RM.get<TimerModel>(name: this.color);
      final duration = _getDuration();
      Timer periodicTimer = Timer.periodic(Duration(seconds: 1), (timer) {
        model.setState((s) {
          s.currentTime -= Duration(seconds: 1);
        });
        model.setState((s) {
          s.circularValue -= 1.0 / double.parse(duration.inSeconds.toString());
        });
      });
      this.periodicTimer = periodicTimer;
    }
  }

  void cancelTimer(Timer timer) {
    if (timer != null) {
      timer.cancel();
    }
  }

  void cancelAllTimers() {
    if (this.timer != null && this.periodicTimer != null) {
      this.timer.cancel();
      this.periodicTimer.cancel();
    }
  }

  void updateCurrentTime() {
    this.currentTime = this.finishTime - this.startTime;
  }
}
