import 'dart:async';

class TimerModel {
  // Duration? _startTime; null safety
  // Duration? _finishTime; null safety

  Duration _startTime;
  Duration _finishTime;

  Duration _currentTime;

  Timer _timer;
  Timer _periodicTimer;

  double _circularValue = 1.0;

  String _color;

  TimerModel(this._startTime, this._finishTime, this._color) {
    _currentTime = _finishTime - _startTime;
  }

  Duration get startTime {
    return _startTime;
  }

  Duration get finishTime {
    return _finishTime;
  }

  void set startTime(Duration startTime) {
    _startTime = startTime;
  }

  void set finishTime(Duration finishTime) {
    _finishTime = finishTime;
  }

  Duration get currentTime {
    return _currentTime;
  }

  void set currentTime(Duration currentTime) {
    _currentTime = currentTime;
  }

  Timer get timer {
    return _timer;
  }

  void set timer(Timer timer) {
    _timer = timer;
  }

  Timer get periodicTimer {
    return _timer;
  }

  void set periodicTimer(Timer timer) {
    _timer = timer;
  }

  set circularValue(double circularValue) {
    _circularValue = circularValue;
  }

  double get circularValue {
    return _circularValue;
  }

  void set color(String color) {
    _color = color;
  }

  String get color {
    return _color;
  }
}
