import 'package:flutter/material.dart';
import 'package:timer_flutter/model/timer_model.dart';
import 'package:states_rebuilder/states_rebuilder.dart';
import 'package:timer_flutter/ui/timer_card.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    // 1ый вариант реализации
    //
    // return Injector(
    //   inject: [Inject(() => TimerModel(Duration(), Duration()))],
    //   builder: (context) => MaterialApp(
    //     title: 'Flutter Demo',
    //     theme: ThemeData(
    //       primarySwatch: Colors.blue,
    //       visualDensity: VisualDensity.adaptivePlatformDensity,
    //     ),
    //     home: MyHomePage(title: 'Flutter Demo Timer'),
    //   ),
    // );
    return MaterialApp(
      title: 'Flutter Demo',
      theme: ThemeData(
        primarySwatch: Colors.blue,
        visualDensity: VisualDensity.adaptivePlatformDensity,
      ),
      home: MyHomePage(title: 'Flutter Demo Timer'),
    );
  }
}

class MyHomePage extends StatefulWidget {
  MyHomePage({Key key, this.title}) : super(key: key);

  final String title;

  @override
  _MyHomePageState createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  Map<String, MaterialColor> cardColors = {
    'red': Colors.red,
    'yellow': Colors.yellow,
    'blue': Colors.blue,
    'green': Colors.green
  };

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: Text(widget.title),
        ),
        body: ListView.builder(
            itemCount: 4,
            itemBuilder: (BuildContext context, int index) {
              final colorNames = cardColors.keys.toList();
              final colors = cardColors.values.toList();

              return Injector(
                inject: [
                  Inject(
                      () =>
                          TimerModel(Duration(), Duration(), colorNames[index]),
                      name: colorNames[index])
                ],
                builder: (context) =>
                    TimerCard(colors[index], colorNames[index]),
              );
            }));
  }
}
